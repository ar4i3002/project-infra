# Dumplings-infrastructure

## Getting started

Разворчаичваем Kubernetes cluster с узлами в яндекс облаке используя terraform:
* Устанавливаем cli yandex для получение credentials - https://cloud.yandex.ru/docs/cli/quickstart
* Устаналиваем terraform либо - https://www.terraform.io/downloads

```bash
terraform plan
terraform apply
```
Укажу сразу на минусы в моей конфигурации 
* Cосояния terraform лучше хранить в s3 bucket, вот в этой докуменации реализация: https://cloud.yandex.ru/docs/tutorials/infrastructure-management/terraform-state-storage
* Credentials облака у меня видны нужно их хранить в другом месте
* В моем конфиге не создается сервисный пользователь для k8s для статичного подключения
* Так же я столкнулся с проблемой при настройке мониторинга prometheus, для prometheus нужно создать роль k8s кластере:https://devopscube.com/setup-prometheus-monitoring-on-kubernetes/

```
Gitlab структура 
- test тестирования сервиса
- project основна ветка разработки
- rw
```

#Устанавливаем: контролер ingress nginx и letsencrip для доступности сервиса и получению сертификатов




Документация: https://cloud.yandex.ru/docs/managed-kubernetes/tutorials/ingress-cert-manager
Внимательно для каждого выпуска сертификата нужно создать свой issuer

## ArgoCD

<img width="300" alt="image" src="https://storage.yandexcloud.net/dumpling-s3/ArgoCD.PNG">


## Мониторинг


<img width="300" alt="image" src="https://storage.yandexcloud.net/dumpling-s3/monitoring.png">


## Полезные матерьялы 

https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes-ru

https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/kubernetes_cluster
