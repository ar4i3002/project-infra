terraform {
  required_providers {
    yandex = {
      source = "terraform-registry.storage.yandexcloud.net/yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.token
  cloud_id  = "b1gt9gv7nev1b44ov5th"
  folder_id = "b1g6urs9j50b92587aqr"
  zone      = "ru-central1-a"
}
resource "yandex_kubernetes_cluster" "zonal_cluster_resource_name" {
  name        = "sarkisyan03"
  description = "dumpligservice"

  network_id = var.network_id

  master {
    version = "1.21"
    zonal {
      zone      = var.yandex_zone
      subnet_id = var.subnet_id
    }

    public_ip = true

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        start_time = "15:00"
        duration   = "3h"
      }
    }
  }

  service_account_id      = var.yandex_iam_service_account
  node_service_account_id = var.yandex_iam_service_account
  release_channel = "RAPID"
  network_policy_provider = "CALICO"
}
resource "yandex_kubernetes_node_group" "my_node_group" {
  cluster_id  = var.subnet_id
  name        = "sarkisyan03"
  description = "description"
  version     = "1.21"

  labels = {
    "key" = "value"
  }

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = true
      subnet_ids         = ["${yandex_vpc_subnet.my_subnet.id}"]
    }

    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = false
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "15:00"
      duration   = "3h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "10:00"
      duration   = "4h30m"
    }
  }
}