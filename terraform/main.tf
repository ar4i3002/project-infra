terraform {
  required_providers {
    yandex = {
      source = "terraform-registry.storage.yandexcloud.net/yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.token
  cloud_id  = "b1gt9gv7nev1b44ov5th"
  folder_id = "b1g6urs9j50b92587aqr"
  zone      = "ru-central1-a"
}
resource "yandex_kubernetes_cluster" "zonal_cluster_resource_name" {
  name        = "sarkisyan03"
  description = "dumpligservice"

  network_id = var.network_id

  master {
    version = "1.21"
    zonal {
      zone      = var.yandex_zone
      subnet_id = var.subnet_id
    }

    public_ip = true

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        start_time = "15:00"
        duration   = "3h"
      }
    }
  }

  service_account_id      = var.yandex_iam_service_account
  node_service_account_id = var.yandex_iam_service_account
  release_channel = "RAPID"
  network_policy_provider = "CALICO"
}