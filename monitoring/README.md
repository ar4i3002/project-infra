# monitoring

Установка prometheus, grafana, alertmanager
1.  helm upgrade --install prometheus  prometheus/
2.  helm upgrade --install grafana  grafana/
3.  helm upgrade --install alertmanager  alertmanager/

* Пароль по умолчанию от Grafana


* admin\admin



Структура чартов

```
├── alertmanager
│   ├── Chart.yaml
│   ├── templates
│   │   ├── _helpers.tpl
│   │   ├── configmap.yaml
│   │   ├── deployment.yaml
│   │   ├── ingress.yaml
│   │   └── services.yaml
│   └── values.yaml
├── grafana
│   ├── Chart.yaml
│   ├── templates
│   │   ├── _helpers.tpl
│   │   ├── deployment.yaml
│   │   ├── ingress.yaml
│   │   ├── pvc.yaml
│   │   └── services.yaml
│   └── values.yaml
└── prometheus
    ├── Chart.yaml
    ├── prom-app-example.yaml
    ├── rules
    │   └── test.rules
    ├── templates
    │   ├── configmap.yaml
    │   ├── deployment.yaml
    │   ├── ingress.yaml
    │   ├── rules.yaml
    │   └── services.yaml
    └── values.yaml
```
